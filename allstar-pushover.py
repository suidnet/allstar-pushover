#!/usr/bin/python

__author__ = 'Justin Harris [KG7FKJ]'
__email__ = 'justin@suid.net'
__status__ = 'Development'
__version__ = 0.01

from urllib import request
from urllib.error import HTTPError, URLError
from pathlib import Path
import subprocess
import socket
import time
import configparser


def get_priv_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('8.8.8.8', 1))
        local_ip = s.getsockname()[0]
        return local_ip
    except Exception:
        return None


def get_pub_ip():
    url = 'https://api.ipify.org'
    try:
        r = request.urlopen(url)
        data = r.readlines()
        return data[0].decode()
    except (URLError, HTTPError):
        return None


def inet_test():
    url = 'https://www.google.com'
    try:
        request.urlopen(url)
    except (URLError, HTTPError):
        return False
    return True


def send_notification(title, message):
    for section in config.sections():
        url = config.get(section, 'url')
        user_key = config.get(section, 'user_key')
        token = config.get(section, 'token')
        data = 'token={}&user={}&title={}&message={}'.format(token, user_key, title, message)

        try:
            request.urlopen(url, data.encode('utf-8')).readlines()[0].decode()
        except (HTTPError, URLError, ValueError):
            continue


def asterisk_running():
    file = '/var/run/asterisk.pid'
    path = Path(file)
    if path.exists():
        return True
    else:
        return False


def get_nodes():
    nodes = []
    cmd = ['/usr/bin/asterisk', '-rx', 'rpt localnodes']
    ret = subprocess.check_output(cmd).decode().split('\n')

    for item in ret:
        try:
            node = int(item)
            if node != 1999:
                nodes.append(node)
            continue
        except ValueError:
            continue

    return nodes


def get_connected_nodes(node):
    conn_nodes = {}
    cmd = ['/usr/bin/asterisk', '-rx', 'rpt lstats {}'.format(node)]
    ret = subprocess.check_output(cmd).decode().split('\n')

    for line in ret[2:]:
        node_row = [x for x in line.split()]
        if len(node_row) != 6:
            continue

        if node_row[5] == 'ESTABLISHED':
            conn_nodes[int(node_row[0])] = {'peer_ip': node_row[1], 'direction': node_row[3]}

    return conn_nodes


def main():
    connected_nodes = {}
    nodes = None
    first_run = True

    while True:
        if first_run is True:
            while get_priv_ip() is None:
                time.sleep(10)

            while inet_test() is False:
                time.sleep(10)

            while asterisk_running() is False:
                time.sleep(10)

            nodes = get_nodes()

            if len(nodes) < 1:
                send_notification('Allstar Node Unknown',
                                  'Your device is online but has no active Allstar nodes configured')
                raise ValueError('Node not configured, check your configuration')
            elif len(nodes) == 1:
                title = 'Allstar Node: {} Online'.format(nodes[0])
            else:
                title = 'Allstar Nodes: {} Online'.format(nodes)

            send_notification(title,
                              'Your Allstar Node(s) are online\n'
                              'Private IP Address: {}\n'
                              'Public IP Address: {}'.format(get_priv_ip(), get_pub_ip()))

            connected_nodes = {x: {} for x in nodes}
            first_run = False

        for node in nodes:
            cur_connected_nodes = get_connected_nodes(node)
            for cn_id, cn_data in cur_connected_nodes.items():
                if cn_id not in connected_nodes.get(node).keys():
                    connected_nodes[node][cn_id] = {**cn_data, 'notification': False}

            title = '[{}] Allstar Node(s) Connected'.format(node)
            message = []
            notify = []
            for cn_id, cn_data in connected_nodes.get(node).items():
                if cn_data['notification'] is False:
                    notify.append(cn_id)
                    if cn_data['direction'] == 'OUT':
                        conn_direction = (node, cn_id)
                    else:
                        conn_direction = (cn_id, node)

                    message.append('Node: {} has connected to node: {}\nPeer IP: {}\n'.format(
                        *conn_direction, cn_data['peer_ip']))

            send_notification(title, ''.join(message))
            for cn_id in notify:
                connected_nodes[node][cn_id]['notification'] = True

            time.sleep(5)

            title = '[{}] Allstar Node(s) Disconnected'.format(node)
            message = []
            cn_remove = []
            for cn_id, cn_data in connected_nodes.get(node).items():
                if cn_id not in get_connected_nodes(node).keys():
                    message.append('Node: {} is disconnected from node: {}\nPeer IP: {}\n'.format(
                       node, cn_id, cn_data['peer_ip']))
                    cn_remove.append(cn_id)

            send_notification(title, ''.join(message))
            for cn_id in cn_remove:
                connected_nodes.get(node).pop(cn_id)

            time.sleep(2)


if __name__ == '__main__':
    config_file = 'pushover.conf'
    path = Path(config_file)
    if not path.exists():
        raise FileNotFoundError('Config file missing')

    config = configparser.ConfigParser()
    config.read(config_file)

    main()
