# Allstar Pushover Module

## Notes
- I have only tested this app on the hamvoip.org image for the Raspberry Pi
- I will be adding functionality to cross reference the node with a call sign. This file is called nodelist.py but is not ready yet.

## Download app and setup a pushover account

1. Download the pushover mobile app on iOS or Android.
2. Setup a pushover account (try the 7-day trial).
3. Take note of the **pushover user key** in the app, you will need this for the pushover.conf file.
4. Create a new app on your pushover account and call it something like "Allstar".
5. Take note of the API Token/Key for the new app you created, you will need this for the pushover.conf file.

## Deploy the allstar-pushover module

Create the following directory on the Raspberry Pi running your node(s):
```bash
mkdir /usr/local/allstar-pushover
```
Copy the allstar-pushover.py and pushover.conf.org file to the /usr/local/allstar-pushover directory.
```bash
cp allstar-pushover.py /usr/local/allstar-pushover
cp pushover.conf.orig /usr/local/allstar-pushover/pushover.conf
```
Modify pushover.conf with your user key and application API token.
```bash
[pushover]
URL = https://api.pushover.net/1/messages.json
TOKEN = token
USER_KEY = user_key
```
Make the application executable:
```bash
chmod 755 /usr/local/allstar-pushover/allstar-pushover.py
```
Modify /etc/rc.local and add the following line before the *exit 0* line:
```bash
/usr/local/allstar-pushover/allstar-pushover.py 2>&1 /dev/null
```