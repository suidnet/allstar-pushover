from urllib import request
from urllib.error import URLError, HTTPError
from html.parser import HTMLParser


class NodeParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.TR = ['#ffffff', '#f1f1f1']
        self.TR_FOUND = False
        self.TD_FOUND = False
        self.NODE_FOUND = False
        self.CALL_FOUND = False
        self.NODE = None
        self.COLUMN_DATA = {}

    def handle_starttag(self, tag, attrs):
        if tag == 'tr':
            for attr in attrs:
                if attr[0] == 'bgcolor' and attr[1] in self.TR:
                    self.TR_FOUND = True

        if tag == 'td' and self.TR_FOUND is True:
            self.TD_FOUND = True

        if tag == 'a' and self.TR_FOUND is True and self.TD_FOUND is True:
            for attr in attrs:
                if attr[0] == 'href' and 'nodeinfo.cgi?' in attr[1]:
                    self.NODE_FOUND = True
                    self.NODE = int(attr[1].split('?')[-1].split('=')[-1])


    def handle_data(self, data):
        if self.TR_FOUND is True and self.TD_FOUND is True:
            if self.NODE_FOUND is True:
                call = data.strip()
                self.COLUMN_DATA[self.NODE] = call

    def handle_endtag(self, tag):
        if tag == 'a':
            if self.NODE_FOUND is True:
                self.NODE_FOUND = False

        if tag == 'td' and self.TD_FOUND is True:
            self.TD_FOUND = False

        if tag == 'tr' and self.TR_FOUND is True:
            self.TR_FOUND = False


class AllstarLink:
    def __init__(self):
        self.url = 'https://allstarlink.org/nodelist.php'
        self.node_list = None

    def update_node_list(self):
        parser = NodeParser()
        try:
            with request.urlopen(self.url) as f:
                parser.feed(str(f.read()))
            self.node_list = parser.COLUMN_DATA
        except (HTTPError, URLError):
            return False

    def get_nodes(self, call):
        if self.node_list is None:
            return None
        nodes = [x for x, y in self.node_list.items() if y.lower() == call.lower()]
        return nodes

    def get_call(self, node):
        if self.node_list is None:
            return None
        return self.node_list[node]
